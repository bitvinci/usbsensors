#! /usr/bin/python
import sys
import usb.core
import time
import math

class DracalUSBBar20(object):
    """ Interface with Dracal Technologies USB-BAR20 barometer.
        May also work with other USB barometers based on the MS5611 chipset,
        but this is currently untested. """

    def __init__(self):
        #
        self.dev = usb.core.find(idVendor=0x289b, idProduct=0x0502)
        if self.dev is None:
            raise OSError('Dracal USB-BAR20 (MS5611) barometer not found - is it connected?')
        self.dev.set_configuration()

    def pressure(self):
        """ interfaces with USB device, fetches pressure data in hPa """
        raw = self.dev.ctrl_transfer(0xC0, 0x10, 0, 0, 8)
        cal1 = self.dev.ctrl_transfer(0xC0, 0x11, 0, 0, 8)
        cal2 = self.dev.ctrl_transfer(0xC0, 0x11, 1, 0, 8)

        raw = raw[1:7]
        cal = cal1[1:7] + cal2[1:7]

        D1 = raw[0] | raw[1]<<8 | raw[2]<<16
        D2 = raw[3] | raw[4]<<8 | raw[5]<<16

        c1 = cal[0] | cal[1]<<8
        c2 = cal[2] | cal[3]<<8
        c3 = cal[4] | cal[5]<<8
        c4 = cal[6] | cal[7]<<8
        c5 = cal[8] | cal[9]<<8
        c6 = cal[10] | cal[11]<<8

        dP = D2 - c5*256.0
        OFF  = c2*65536.0 + c4*dP/128.0
        SENS = c1*32768.0 + c3*dP/256.0
        P = (D1*SENS/2097152.0 - OFF)/32768.0/100.0 # mbars

        return P

    def pressure_height(self, Po=1013.0, To = 280.0):
        """ calculates pressure height relative to a sea-level standard pressure Po (hPa),
            and sea-level standard temperature (K) """
        R = 8.3144598 # universal gas constant
        M = 0.0289644 # molar mass of dry air
        g = 9.80665   # earth surface gravitational acceleration
	P = self.pressure()
        dH = -R*To/g/M*math.log(P/Po)
        return dH
