# usbsensors
Python module for interfacing with various types of environment sensors over USB.  Works with Dracal Tech. USB Barometer, USB-BAR20.  If we receive samples of other USB connected sensors, we will work on adding support for them too.

## Dependencies
The only dependecy is the python USB access module, PyUSB.
You can install it using `pip`,

```
$ sudo pip install pyusb
```

or from your distro repository. E.g. Debian / Ubuntu can install with,

```
$ sudo apt-get install python-usb
```

## Trying it out

You can test the `usbsensors` module without installation.
Fetch a fresh clone of the repository,

```
$ git clone https://gitlab.com/bitvinci/usbsensors
```

### Reading data from Dracal USB-BAR20

From the repository base directory, open upp a python shell and
execute the following commands,

```python

from usbsensors import DracalUSBBar20
barometer = DracalUSBBar20()

print( barometer.pressure() + "hPa" )
print( barometer.pressure_height() + "m" )

```

Note that if no Dracal USB-BAR20 device is found on the USB bus,
the `DracalUSBBar20` constructor will throw an `OSError`.

## TODO-list
 * Add `setup.py` setuptools file for easy installation `$ sudo setup.py install`
 * Register with `pip` for even easier installation `$ sudo pip install usbsensors`
 * Implement Asbstract Base Class to enforce a consistent interface to different devices.
 * Add further support for other sensors (when we receive them)
 * Implement device calibration methods
 * Implement baseline unit tests

### Credit-list

 * Code for reading the Dracal USB-BAR20 device was ported from the usbtenki development in `C`,
   https://github.com/Dracaltech/usbtenki, by Raphaël Assénat.
